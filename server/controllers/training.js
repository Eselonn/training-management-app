const db = require('../database.js');
const jwt = require('jsonwebtoken');
const crypto = require("crypto");
const secret = 'secret';
const bcrypt = require('bcrypt');
const nodeEmail = require("../email");

var allTrainings = [];
var allUsers = [];

const loadAllTraining = () => {
    db.connection.query(`SELECT * from trainings`, (err, results, fiels) => {
        if(err) {
            throw(new Error("Database error!"));
        }
        if(results.length > 0) {
            allTrainings = [];
            for(var i = 0; i<results.length; i++)
            {
                const date = new Date(results[i].date);
                const dayNames = [ 'Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'];
                const day = dayNames[date.getDay()];
                const name = getUsername(results[i].owner_id);
                allTrainings.push({
                    id:results[i].id,
                    day,
                    date:(date.getFullYear()+ '/' + (date.getMonth() + 1) + '/' +  date.getDate()).toString(),  
                    desc:results[i].description,  
                    comment:results[i].comment,  
                    owner_id:results[i].owner_id,
                    owner_name: name,
                    realisation:results[i].realisation,  
                    status:results[i].status === -1 ? 'Aktywny': 'Zakonczony', 
                })
            }
        }
    })
}


const loadAllUsers = () => {
    db.connection.query(`SELECT * from users`, (err, results, fiels) => {
        if(err) {
            console.log(err);
            res.status(401).json({errorDesc: "B??d bazy danych!"});
        }
        if(results.length > 0) {
            allUsers = [];
            for(var i = 0; i<results.length; i++)
                allUsers.push({
                    name: (results[i].firstName + ' ' + results[i].lastName), 
                    id: results[i].id,
                    active: results[i].registerCode === '' ? 'Zarejestrowany': "Niezarejestrowany",
                    regCode: results[i].registerCode, 
                    email: results[i].email})
        } 
    })
}

loadAllUsers();
loadAllTraining();

const getUserAllTraining = (user_id) => {
    const trainingList = [];
    for(let i = 0; i<allTrainings.length; i++) {
        if(allTrainings[i].owner_id === user_id) 
            trainingList.push(allTrainings[i]);
    }

    return trainingList;
}


const updateUserTraining = (training) => {
    console.log(training);
    for(let i = 0; i < allTrainings.length; i++) {
        if(allTrainings[i].id === training.id) 
            allTrainings[i] = training;
    }
}

const getAllActiveTrainings = () => {
    const allActiveTrainings = [];
    for(let i = 0; i < allTrainings.length; i++) {
        if(allTrainings[i].status === 'Aktywny')
            allActiveTrainings.push(allTrainings[i]);   
    }
    return allActiveTrainings;
}

const getUserId = (username) => {
    const found = allUsers.find(user => user.name === username);
    return found.id;
}

const getUserIdByEmail = (email) => {
    const found = allUsers.find(user => user.email === email);
    return found.id;
}

const getUsername = (id) => {
    const found = allUsers.find(user => user.id === id)
    return found.name;
}

const deleteUser = (email) => {
    const newUserList = [];
    for(let i = 0; i < allUsers.length; i++)
        if(allUsers[i].email !== email)
            newUserList.push(allUsers[i]);
        
    allUsers = newUserList;
}

const updateUser = data => {
    var found = allUsers.find(user => user.regCode === data.regCode);
    found = Object.assign({}, data, {regCode:''});
}

module.exports = {
    getUserTraining: (req, res) => {
        const { token } = req.body;
        const decoded = jwt.verify(token, secret);
        const user_id = decoded.id;
        const userTraining = getUserAllTraining(user_id);
        res.json(userTraining).status(200).end();
    },
    updateTraining: (req, res) => {
        const { token, training} = req.body;
        if(token && training) {
            const date = new Date(training.date);
            db.connection.query('UPDATE trainings SET date = ?, description = ?, comment = ?, realisation = ?, status = ?, owner_id = ? WHERE id = ?', [date, training.desc, training.comment, training.realisation, training.status, training.owner_id, training.id], (err, results, fiels) => {
                if(err)  {
                    console.log(err)
                    res.status(401).end();
                }
                else {
                    if(training.status === 1) {
                        training.date = (date.getFullYear()+ '/' + (date.getMonth() + 1) + '/' +  date.getDate()).toString();
                        updateUserTraining(Object.assign({}, training, {status: 'Zakonczony'}));
                    }
                    else updateUserTraining(training);
                    res.status(200).end();
                }
            })
        }
    },
    loadAdminData: (req, res) => {
        const { token } = req.body;
        const decoded = jwt.verify(token, secret);
        if(decoded.admin === true) {
            const allActiveTrainings = getAllActiveTrainings();
            res.json({
                allActiveTrainings,
                allTrainings,
                allUsers
            }).status(200).end();
        }
    },
    addTraining: (req, res) => {
        const { token, training } = req.body;
        if(training && token) {
            const decoded = jwt.verify(token, secret);
            const user_id = getUserId(training.user);
            if(decoded.admin === true && user_id !== undefined) {
                db.connection.query('INSERT INTO trainings (date, description, comment, owner_id, realisation) VALUES (?, ?, ?, ?, "")', [training.date, training.description, training.comment, user_id], (err, results, fiels) => {
                    if(err)  {
                        console.log(err)
                        res.status(401).end();
                    }
                    else {
                        loadAllTraining();
                        res.status(200).end();
                    }
                }); 
            }
        }
    },
    deleteUser: (req, res) => {
        const { token, user} = req.body;
        if(user && token) {
            const decoded = jwt.verify(token, secret);
            if(decoded.admin === true) {
                const user_id = getUserIdByEmail(user.email);
                db.connection.query(`DELETE FROM trainings WHERE owner_id = ?;`, user_id, (err, results, fiels) => {
                    if(err)  {
                        console.log(err)
                        res.status(401).end();
                    }
                });
                db.connection.query(`DELETE FROM users WHERE email = ?;`, user.email, (err, results, fiels) => {
                    if(err) {
                        res.send(400);
                    } else {
                        deleteUser(user.email)
                        res.status(200).end();
                    }
                });
            }
        }
    }, 
    addUser: (req, res) => {
        const { token, user} = req.body;
        if(user && token) {
            const decoded = jwt.verify(token, secret);
            if(decoded.admin === true) {
                const found = allUsers.find(singleUser => singleUser.email === user.email);
                if(found === undefined) {
                    var id = crypto.randomBytes(20).toString('hex');
                    db.connection.query(`INSERT INTO users (email, registerCode, firstName, lastName, admin, password) values (?, ?, "", "", 0, "")`, [user.email, id], (err, results, fiels) => {
                        if(err)  {
                            console.log(err)
                            res.status(401).end();
                        }
                        else {
                            nodeEmail.registerMail(user.email, id);
                            res.status(200).end();
                            loadAllUsers();
                        }
                    });
                }
            }
        }
    },
    regkeyvalidation: (req, res) => {
        const regkey = req.body.regkey;
        if(regkey) {
            db.connection.query(`SELECT * from users WHERE registerCode = ?`, [regkey], (err, results, fiels) => {
                if(err) {
                    console.log(err);
                    res.status(401).json({errorDesc: "B??d bazy danych!"});
                    return;
                }
                if(results.length > 0) {
                        res.json({email:results[0].email}).status(200);
                } else {
                    res.status(401);
                }
            });
        } else {
            res.status(401).json({errorDesc: "Brak danych..."});
        }
    },
    finishSignUp: (req, res) => {
        const {firstName, lastName, password, email, regCode} = req.body;
        let hash = bcrypt.hashSync(password, 10);

        db.connection.query('UPDATE users SET firstName = ?, lastName = ?, password = ?, registerCode = "" WHERE email = ?',[firstName, lastName, hash, email], (err, results, fiels) => {
            if(err)  {
                console.log(err)
                res.status(401).end();
            }
            else {
                updateUser({firstName, lastName, password, email, regCode});
                res.send(200);
                loadAllUsers();
            }
        })
    },
    deleteTraining: (req, res) => {
        const { token, training} = req.body;
        if(training && token) {
            const decoded = jwt.verify(token, secret);
            if(decoded.admin === true) { 
                db.connection.query('DELETE FROM trainings WHERE id=?', [training.id], (err, results, fiels) => {
                    if(err) {
                        console.log(err);
                        res.status(401).end();
                    } else {
                        loadAllTraining();
                        res.send(200);
                    }
                })
            }
        }
    }
}