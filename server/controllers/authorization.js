const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const secret = 'secret';
const crypto = require("crypto");
const db = require('../database');


module.exports = {
    signin: (req, res) => {
        const {password, email} = req.body;
        console.log(email);
        if(email && password) {
            db.connection.query(`SELECT * from users WHERE email = ?`, [email], (err, results, fiels) => {
                if(err) {
                    console.log(err);
                    res.status(401).json({errorDesc: "Błąd bazy danych!"});
                } else {
                    if(results.length > 0) {
                        console.log(results[0]);
                        if(bcrypt.compareSync(password, results[0].password)) {

                            if(results[0].admin == 1) {
                                const token = jwt.sign({email, id: results[0].id, admin: true}, secret, {
                                    expiresIn: 60*60
                                });
                                res.send({token, admin: true}).status(200);
                            }
                            else {
                                const token = jwt.sign({email, id: results[0].id, admin: false}, secret, {
                                    expiresIn: 60*60
                                });
                                res.send({token, admin: false}).status(200);
                            }
                            res.end();
                        } else res.status(401).json({errorDesc: "Wprowadzone dane są nieprawidłowe!"});
                    } else res.status(401).json({errorDesc: "Wprowadzone dane są nieprawidłowe!"});
                }
            });
        } else { 
            res.status(401).json({errorDesc: "Wprowadzone dane są nieprawidłowe!"}).
            res.end();
        }
    },
    getUserInfo: (req, res) => {
        const {token} = req.body;
        if(token) {
            const decoded = jwt.verify(token, secret);
            res.json({admin:decoded.admin}).status(200).end();
        }
    }
}