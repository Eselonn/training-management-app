const express = require('express');
const routes = express.Router();
const auth = require('../controllers/authorization');
const training = require('../controllers/training');

routes.post('/signin', auth.signin);
routes.post('/getUserTraining', training.getUserTraining);
routes.post('/updateTraining', training.updateTraining);
routes.post('/loadAdminData', training.loadAdminData);
routes.post('/addTraining', training.addTraining);
routes.post('/deleteUser', training.deleteUser);
routes.post('/addUser', training.addUser);
routes.post('/finishSignUp', training.finishSignUp);
routes.post('/regkeyvalidation', training.regkeyvalidation);
routes.post('/deleteTraining', training.deleteTraining);
routes.post('/getUserInfo', auth.getUserInfo);


module.exports = routes;