const nodemailer = require('nodemailer');
const db = require('./database');

const emailService = nodemailer.createTransport({
    service: 'gmail',
    auth: {
    }
})

module.exports = {
    registerMail: (email, code) => {
        options = {
            from: 'trening24422@gmail.com',
            to: email,
            subject: 'Rejestracja w serwisie trenerskim',
            html: 'Witaj, </br> Zostajesz zaproszony do menadżera treningow. </br> Aby dokonczyc rejestracje wejdz pod ten link: <a href="http:/localhost:3000/register/' + code + '"> CLICK </a>',
        }
    
        db.connection.query("UPDATE users SET registerCode=? WHERE email=?", [code, email], (err, results, fiels) => {
            if(err) console.log(err);
        });
    
        emailService.sendMail(options, function(error, info){
            if (error) {
              console.log(error);
            } else {
              console.log('Email sent: ' + info.response);
            }
        })
    }
}