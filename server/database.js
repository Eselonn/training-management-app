const mysql = require('mysql');
const mysqlConnection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'trener'
});

mysqlConnection.connect(function(err) {
    if (err) throw err;
});

module.exports = {connection: mysqlConnection}