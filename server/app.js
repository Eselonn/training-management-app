const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const app = express();
app.use(cookieParser());
app.use(cors({credentials: true, origin: 'http://localhost:3000'}));
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json());


const routes = require('./routes/routes');
app.use('/', routes);

app.listen(3005, ()=> console.log('Server started on port 3005'))
