import { combineReducers } from 'redux';
import userReducers from './app/user/duck';
import modalReducers from './app/modal/duck';

const rootReducers = combineReducers({
    user: userReducers,
    modal: modalReducers
})

export default rootReducers;