import modalReducers from './reducers';
export {default as modalTypes} from './types';
export {default as modalActions} from './actions';


export default modalReducers;