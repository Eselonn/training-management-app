import types from './types';

const openModal = (modalId, props) => ({
    type: types.OPEN_MODAL,
    props,
    modalId
})

const closeModal = () => ({
    type: types.CLOSE_MODAL
})


export default {
    openModal,
    closeModal
}