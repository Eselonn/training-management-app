import types from './types';

const INITIAL_STATE = {
    activeModalId: null,
    activeModalProps: null
}

const modalReduers = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case types.OPEN_MODAL:
            return {activeModalId: action.modalId, activeModalProps: action.props};
        case types.CLOSE_MODAL:
            return INITIAL_STATE;
        default: return state;
    }
}

export default modalReduers;