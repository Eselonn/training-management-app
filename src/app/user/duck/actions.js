import types from './types';
import API from '../../../utils/API';

const addUser = (data) => ({
    type: types.ADD_USER,
    data
});

const userLogin = () => ({
    type: types.USER_LOGIN
})

const loginFail = (error) => ({
    type: types.USER_LOGIN_ERROR,
    error
})

const userLogout = () => ({
    type: types.REMOVE_USER
})

const userLoadData = () => ({
    type: types.UPDATE_USER
})

const userLoadTraining = training => ({
    type: types.UPDATE_USER_TRAINING,
    training
})

const userRole = data => ({
    type: types.USER_SET_ROLE,
    data
})

const fetchError = () => ({
    type: types.FETCH_ERROR
})

const login = (email, password, history) => {
    return async dispatch => {
        dispatch(userLogin())
        await API.post(`/signin`, {email, password})
        .then(res => {
            localStorage.setItem('usertoken', res.data.token);
            dispatch(addUser(res.data));
            if(res.data.admin === true) {
                dispatch(loadTraining('admin', res.data.token));
            }
            else {
                dispatch(loadTraining('user', res.data.token));
            }
            history.push('/home')
        })
        .catch(err => {
            if(err.response)
               dispatch(loginFail(err.response.data.errorDesc));
            else dispatch(loginFail('Wystąpił błąd bazy danych. Zapraszamy za jakis czas.'));
        })
    }
}

const loadUserData = (token) => {
    return async dispatch => {
        dispatch(userLoadData());
        await API.post('/getUserInfo', {token})
        .then(res => {
            dispatch(userRole(res.data));

            if(res.data.admin === true) dispatch(loadTraining('admin', token))
            else dispatch(loadTraining('user', token));
        })
        .catch(err => {
            localStorage.removeItem('usertoken');
            localStorage.removeItem('role');
            dispatch(userLogout());
        })
    }
}

const loadTraining = (userType, token) => {
    return async dispatch => {
        if(userType === 'user') {
            await API.post('/getUserTraining', {token})
            .then(res => {
                dispatch(userLoadTraining(res.data));
            })
            .catch(err => {
                localStorage.removeItem('usertoken');
                localStorage.removeItem('role');
                dispatch(userLogout());
            })
        } else if(userType === 'admin') {
            await API.post('/loadAdminData', {token})
            .then(res => {
                dispatch(userLoadTraining(res.data));
            })
            .catch(err => {
                localStorage.removeItem('usertoken');
                localStorage.removeItem('role');
                dispatch(userLogout());
            })
        }
    }
}

const logout = () => {
    localStorage.removeItem('usertoken');
    localStorage.removeItem('role');
    return async dispatch => {
        dispatch(userLogout());
    }
}

///APP FUNCTIONS

const reloadUserList = (data) => ({
    type: types.RELOAD_USER_LIST,
    data
})


const closeTraining = (allTraining, selectedTraining) => {
    return async dispatch => {
        dispatch(userLoadData());
        await API.post('/updateTraining', {training: Object.assign({}, selectedTraining, {status: 1}), token: localStorage.usertoken})
        .then(res => {
            dispatch(userLoadTraining([...allTraining]));
        })
        .catch(err => {
            dispatch(fetchError());

        })

    }
}


const adminCloseTraining = (selectedTraining) => {
    return async dispatch => {
        dispatch(userLoadData());
        await API.post('/updateTraining', {training: Object.assign({}, selectedTraining, {status: 1}), token: localStorage.usertoken})
        .then(res => {
            dispatch(loadTraining('admin', localStorage.usertoken));
        })
        .catch(err => {
            dispatch(fetchError());
        })

    }
}

const saveTraining = (allTraining, selectedTraining) => {
    return async dispatch => {
        dispatch(userLoadData());
        await API.post('/updateTraining', {training: selectedTraining, token: localStorage.usertoken})
        .then(res => {
            dispatch(userLoadTraining([...allTraining]));
        })
        .catch(err => {
            dispatch(fetchError());
        })
    }
}

const addTraining = (training) => {
    return async dispatch => {
        await API.post('/addTraining', {training, token: localStorage.usertoken})
        .then(res => {
            dispatch(loadTraining('admin', localStorage.usertoken));
        })
    }
}

const removeUserFromApp = (user, newList) => {
    return async dispatch => {
        dispatch(userLoadData());
        await API.post('/deleteUser', {user, token: localStorage.usertoken})
        .then(res => {
            dispatch(reloadUserList([...newList]));
        })
        .catch(err => {
            dispatch(fetchError());
        })
    }
}

const addNewUser = (user, newList) => {
    return async dispatch => {
        dispatch(userLoadData());
        await API.post('/addUser', {user, token: localStorage.usertoken})
        .then(res => {
            dispatch(reloadUserList([...newList]));
        })
        .catch(err => {
            dispatch(fetchError());
        })
    }
}

const editTraining = (training) => {
    return async dispatch => {
        dispatch(userLoadData());
        await API.post('/updateTraining', {training, token: localStorage.usertoken})
        .then(res => {
            dispatch(loadTraining('admin', localStorage.usertoken));
        })
        .catch(err => {
            dispatch(fetchError());
        })
    }
}

const deleteTraining = training => {
    return async dispatch => {
        dispatch(userLoadData());
        await API.post('/deleteTraining', {training, token: localStorage.usertoken})
        .then(res => {
            dispatch(loadTraining('admin', localStorage.usertoken));
        })
        .catch(err => {
            dispatch(fetchError());
        })
    }
}

export default {
    login,
    logout,
    loadTraining,
    closeTraining,
    saveTraining,
    loadUserData,
    addTraining,
    removeUserFromApp,
    addNewUser,
    adminCloseTraining,
    editTraining,
    deleteTraining
}