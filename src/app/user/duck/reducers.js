import types from './types';

const INITIAL_STATE = {
    loading: false,
    trainingList: [],
    error: '',
    accountType: 'user'
}

const userReduceres = (state = INITIAL_STATE, actions) => {
    switch(actions.type) {
        case types.USER_LOGIN:
            return Object.assign({}, state, {loading: true});
        case types.USER_LOGIN_ERROR: 
            return Object.assign({}, state, {loading: false, error: actions.error})
        case types.REMOVE_USER:
            return Object.assign({}, state);
        case types.ADD_USER:
            return Object.assign({}, state, {loading: false, ...actions.data});
        case types.UPDATE_USER_TRAINING:
            return Object.assign({}, state, {loading: false, trainingList: actions.training });
        case types.UPDATE_USER:
            return Object.assign({}, state, {loading: true});
        case types.USER_SET_ROLE:
            return {...state, accountType: actions.data.admin === true ? 'admin':'user'}
        case types.RELOAD_USER_LIST:
            return {...state, loading: false, trainingList: {...state.trainingList, allUsers: actions.data}}
        case types.FETCH_ERROR: 
            return {...state, loading: false}
        default: return state;
    }
}

export default userReduceres;