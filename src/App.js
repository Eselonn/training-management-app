import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Login from './components/Login';
import Register from './components/Register';
import Home from './components/Home';
import Spinner from './components/Spinner';
import './App.scss';

function App() {
  return (
    <Router basename='/'>
      <Route path='/home' component={Home}/>
      <Route path='/login' component={Login}/>
      <Route path='/register/:regkey' component={Register}/>
      <Route path='/' exact component={Home}/>

      <Spinner/>
    </Router>
  );
}

export default App;
