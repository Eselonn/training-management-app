import React from 'react';
import TrainingTable from '../TrainingTable';
import './style.scss';


const TrainingContainer = (props) => {
    return (
        <div className='table-container'>
            <h2>{props.title}</h2>
            <TrainingTable adminColumn={props.adminColumn} trainingList={props.training}/>
        </div>
    )
}

export default TrainingContainer;