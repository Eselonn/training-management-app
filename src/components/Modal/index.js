import React from 'react';
import { connect } from 'react-redux';
import { modalActions } from '../../app/modal/duck';
import { Modal, Button } from 'react-bootstrap';
import './style.scss';

const ModalContainer = (props) => {
    const {hideModal, saveChanges, activeModalId, modalId} = props;
    return (
        <Modal show={modalId === activeModalId? true:false}>
            <Modal.Header closeButton onClick={() => hideModal()}>
                <Modal.Title>{props.title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>{props.children}</Modal.Body>
            <Modal.Footer>
                <Button  variant="secondary" onClick={() => hideModal()}>
                    Zamknij
                </Button>
                <Button className="default-button-color" onClick={() => saveChanges()}>
                    Zapisz
                </Button>
                </Modal.Footer>
        </Modal>
    )
}


const mapStateToProps = state => ({
    activeModalId: state.modal.activeModalId
})

const mapDispatchToProps = dispatch => ({
    hideModal: () => dispatch(modalActions.closeModal())
})

export default connect(mapStateToProps, mapDispatchToProps)(ModalContainer);