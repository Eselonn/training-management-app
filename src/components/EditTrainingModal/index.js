import React from "react";
import Modal from '../Modal';
import { Form } from 'react-bootstrap'
import DatePicker from "react-datepicker";
import { connect } from 'react-redux';
import {userActions} from '../../app/user/duck'
import { modalActions } from '../../app/modal/duck'

import "react-datepicker/dist/react-datepicker.css";

class EditTrainingModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: this.props.activeModalProps.owner_name,
            description: this.props.activeModalProps.desc,
            comment: this.props.activeModalProps.comment,
            date: new Date(this.props.activeModalProps.date),
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleAddTraining = this.handleAddTraining.bind(this);
        this.handleDataChange = this.handleDataChange.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value })
    }

    handleDataChange(date) {
        this.setState({
            date
        })
    }

    handleAddTraining() {
        const {user, description, comment, date} = this.state;
        if(user !== '') {
            this.props.editTraining(Object.assign({}, this.props.activeModalProps, {user, desc: description, comment, date}));
            this.props.hideModal();
        }
    }

    renderUsers() {
        return this.props.userTraining.allUsers.map((user, id) => {
            if(user.active !== 'Niezarejestrowany') return  <option key={id}> {user.name} </option>
            else return null;
        })
    }

    render() {
        if(this.props.userTraining.length === 0) return null;
        const {date, user, description, comment} = this.state;
        return (
            <Modal
                saveChanges={() => this.handleAddTraining()}
                title="Dodawanie Treningu" 
                modalId='edit-training-modal'>
                <Form>
                    <Form.Group>
                        <Form.Label> Zawodnik </Form.Label>
                        <Form.Control 
                            value={user}
                            name='user' 
                            onChange={(e) => this.handleChange(e)}  
                            as="select">
                                <option> </option>
                                {this.renderUsers()}
                        </Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label> Data </Form.Label>
                        <DatePicker
                            name="date"
                            onChange={(e) => this.handleDataChange(e)}  
                            selected={date}
                        />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Tresc treningu</Form.Label>
                        <Form.Control
                            value={description}
                            name='description' 
                            onChange={(e) => this.handleChange(e)}  
                            as="textarea" 
                            rows="3" />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Uwagi trenera</Form.Label>
                        <Form.Control
                            value={comment}
                            name='comment' 
                            onChange={(e) => this.handleChange(e)}  
                            as="textarea" 
                            rows="3" />
                    </Form.Group>
                </Form>
            </Modal>
        )
    }
}

const mapStateToProps = state => ({
    userTraining: state.user.trainingList,
    activeModalProps: state.modal.activeModalProps
})

const mapDispatchToProps = dispatch => ({
    editTraining: (training) => dispatch(userActions.editTraining(training)),
    hideModal: () => dispatch(modalActions.closeModal()),

})

export default connect(mapStateToProps, mapDispatchToProps)(EditTrainingModal)