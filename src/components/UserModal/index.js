import React from 'react';
import Modal from '../Modal';
import {Form} from 'react-bootstrap'
import { connect } from 'react-redux';
import { modalActions } from '../../app/modal/duck'
import { userActions } from '../../app/user/duck'
class UserModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            realisation: ''
        }
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value })
    }

    addUserRealisationToTraining() {
        let selectedTraining = this.props.activeModalProps;
        const allTraining = this.props.allTraining;
        let found = allTraining.find(training => training.id === selectedTraining.id);
        found.realisation = this.state.realisation;
        this.props.saveTraining(allTraining, found)
        this.props.hideModal();
    }

    render() {
        return (
            <Modal
                saveChanges={() => this.addUserRealisationToTraining()} 
                title="Realizacja treningu" 
                modalId='user-modal-realisation'>
                <Form>
                    <Form.Group>
                        <Form.Control 
                            onChange={(e) => this.handleChange(e)} 
                            value={this.state.realisation} 
                            name='realisation' 
                            as="textarea" 
                            rows="3" />
                    </Form.Group>
                </Form>
            </Modal>
        )
    }
}

const mapStateToProps = state => ({
    activeModalId: state.modal.activeModalId,
    activeModalProps: state.modal.activeModalProps,
    allTraining: state.user.trainingList
})

const mapDispatchToProps = dispatch => ({
    hideModal: () => dispatch(modalActions.closeModal()),
    saveTraining: (allTrainings, selected) => dispatch(userActions.saveTraining(allTrainings, selected)),

})

export default connect(mapStateToProps, mapDispatchToProps)(UserModal);