import React from 'react';
import Header from '../Header';
import { Button } from 'react-bootstrap';
import TableContainer from '../TrainingContainer';
import {connect} from 'react-redux';
import AddTrainingModal from '../AddTrainingModal';
import EditTrainingModal from '../EditTrainingModal';
import UserManagementModal from '../UserManagementModal';
import { modalActions } from '../../app/modal/duck'
import AdminDropdown from '../AdminDropdown';

class AdminPanel extends React.Component {

    addOptions(list) {
        if(!list) return;
        const newList = [];
        for(let i = 0; i<list.length; i++) 
            newList.push(Object.assign({}, list[i], {option: <AdminDropdown training={list[i]}/>}));
        return newList;
    }

    render() {
        return (
        <React.Fragment>
            <Header
                title="Zarządzaj treningami">
                <Button onClick={() => this.props.openModal('add-training-modal', null)}> Dodaj trening  </Button>
                <Button onClick={() => this.props.openModal('user-management-modal', null)}> Zarzadzaj uzytkownikami </Button>
            </Header>
            {this.props.userTraining.length !== 0 ? <TableContainer title='Lista wszystkich treningow' adminColumn={true} training={this.addOptions(this.props.userTraining.allTrainings)}/>:''}
            {this.props.userTraining.length !== 0 ? <TableContainer title='Lista wszystkich aktywnych treningow' adminColumn={true} training={this.addOptions(this.props.userTraining.allActiveTrainings)}/>:''}
            <AddTrainingModal/>
            <UserManagementModal/>
            {this.props.activeModalProps !== null? <EditTrainingModal/> : null}
        </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    userTraining: state.user.trainingList,
    activeModalProps: state.modal.activeModalProps

})

const mapDispatchToProps = dispatch => ({
    openModal: (modalId, modalProps) => dispatch(modalActions.openModal(modalId, modalProps))

})

export default connect(mapStateToProps, mapDispatchToProps)(AdminPanel);