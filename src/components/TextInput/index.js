import React from 'react';
import PropTypes from 'prop-types';
import {Form} from 'react-bootstrap'

import './style.scss';

const TextInput = (props) => {
    return (
        <Form.Group>
            <Form.Label>{props.title}</Form.Label>
            <Form.Control 
                required  
                name={props.name} 
                onChange={props.onChange} 
                placeholder={props.placeholder}
                type={props.type}
                value={props.itemValue}
                disabled={props.disabled}
                minLength={props.minLength}/>
        </Form.Group>
    )
}


TextInput.propTypes = {
    title: PropTypes.string,
    onChange: PropTypes.func,
    name: PropTypes.string,
    type: PropTypes.string,
    placeholder: PropTypes.string,
    disabled: PropTypes.bool,
    minLength: PropTypes.number
}

export default TextInput;