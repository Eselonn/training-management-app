import React from 'react';
import { connect } from 'react-redux';
import './style.scss';

const Spinner = (props) => {
    if(props.isSpinnerActive === false)
        return null;

    return (
    <div className="spinner-container">
        <div className="spinner"></div>
    </div>)
}


const mapPropsToState = state => ({
    isSpinnerActive: state.user.loading
})


export default connect(mapPropsToState, null)(Spinner);