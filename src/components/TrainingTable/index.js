import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import filterFactory, { textFilter  } from 'react-bootstrap-table2-filter';

import 'react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';

class TrainingTable extends React.Component {
    render() {
        const columns = [{
            dataField: 'id',
            hidden: true
        }, {
            dataField: 'date',
            text: 'Data'
        }, {
            dataField: 'day',
            text: 'Dzień'
        }, {
            dataField: 'desc',
            text: 'Treść treningu'
        }, {
            dataField: 'comment',
            text: 'Uwagi trenera'
        }, {
            dataField: 'realisation',
            text: 'Realizacja'
        }, {
            dataField: 'status',
            text: 'Status'
        }, {
            dataField: 'options',
        }
    ]

    const adminColumns = [
        {
            dataField: 'id',
            hidden: true
        } ,{
            dataField: 'date',
            text: 'Data'
        }, {
            dataField: 'owner_name',
            text: 'Zawodnik',
            filter: textFilter()
        }, {
            dataField: 'desc',
            text: 'Treść treningu'
        }, {
            dataField: 'comment',
            text: 'Uwagi trenera'
        }, {
            dataField: 'realisation',
            text: 'Realizacja'
        }, {
            dataField: 'status',
            text: 'Status'
        }, {
            dataField: 'option',
        }
    ]
        
        return (
            <BootstrapTable 
                striped  
                keyField='id' 
                data={this.props.trainingList} 
                columns={this.props.adminColumn === true? adminColumns:columns} 
                filter={ filterFactory() }
                pagination={  paginationFactory({ 
                    sizePerPageList: [ {
                        text: '5', value: 5
                      }, {
                        text: '10', value: 10
                      }, {
                        text: '25', value: 25
                      } ]

                }) } />
        )
    }
}

export default TrainingTable;