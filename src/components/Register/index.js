import React, {Component} from "react";
import {Form, Button} from 'react-bootstrap';
import TextInput from '../TextInput';
import API from "../../utils/API";
import './style.scss';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName:'',
            lastName:'',
            email: '',
            password: '',
            repassword: '',
            errorMessage: '',
            regCode: ''
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const {name, value} = e.target;
        this.setState({[name]: value});
    }

    handleSubmit= async (e) => {
        e.preventDefault();
        const {firstName, lastName, email, password, repassword} =  this.state;
        if(password !== repassword) {
            this.setState({errorMessage: 'Hasla nie pasuja do siebie!'})
            return;
        }
        
        await API.post('/finishSignUp', {firstName, lastName, email, password})
        .then(res => this.props.history.push('/login'))
        .catch(err => console.log(err));
    }

    async componentDidMount() {
       await API.post('/regkeyvalidation', {regkey: this.props.match.params.regkey})
        .then(res => this.setState({email: res.data.email, regCode: this.props.match.params.regkey}))
        .catch(err => console.log(err))
    }

    render() {
        const {firstName, lastName, email, password, errorMessage, repassword} = this.state;
        return (<div className="signin-container">
        <Form className="signin-form" onSubmit={ this.handleSubmit}>
            <h2>Rejestracja</h2>
            <TextInput 
                name='firstName' 
                onChange={(e) => this.handleChange(e)}  
                placeholder='Wpisz swoje imie...' 
                title='Imie:' 
                type='text'
                minLength={3}
                itemValue={firstName}/>
            <TextInput 
                name='lastName' 
                onChange={(e) => this.handleChange(e)}  
                placeholder='Wpisz swoje nazwisko...' 
                title='Nazwisko:' 
                type='text'
                minLength={3}
                itemValue={lastName}/>
             <TextInput 
                name='email' 
                onChange={(e) => this.handleChange(e)}  
                placeholder='Wpisz swoj email...' 
                title='Imie:' 
                type='email'
                disabled={true}
                minLength={4}
                itemValue={email}/>   
            <TextInput 
                name='password' 
                onChange={(e) => this.handleChange(e)}  
                placeholder='Wpisz swoje haslo...' 
                title='Haslo:' 
                type='password'
                minLength={5}
                itemValue={password}/>
            <TextInput 
                name='repassword' 
                onChange={(e) => this.handleChange(e)}  
                placeholder='Wpisz ponownie haslo...' 
                title='Powtorz haslo:' 
                type='password'
                minLength={5}
                itemValue={repassword}/>
            <Button className="default-button-color" type="submit">
                Rejestruj
            </Button>
                <p className="error-msg">{errorMessage}</p>
            </Form>
        </div>
        )
    }
}
export default Register;
