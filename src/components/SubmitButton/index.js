import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

const SubmitButton = (props) => {
    return <input 
        value={props.value}
        className='submit-button'
        type='submit'/>
}


SubmitButton.propTypes = {
    value: PropTypes.string,
}

export default SubmitButton;