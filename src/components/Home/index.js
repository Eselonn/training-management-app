import React from 'react';
import { connect } from 'react-redux';
import UserPanel from '../UserPanel';
import AdminPanel from '../AdminPanel';
import {userActions} from '../../app/user/duck';

import './style.scss';

class Home extends React.Component {
    componentDidMount() {
        if(localStorage.usertoken)
            this.props.loadUser(localStorage.usertoken);
        else this.props.history.push('./login');
    }

    render() {
        if(this.props.accountType === 'admin')
            return <AdminPanel/>
        else if(this.props.accountType === 'user') return <UserPanel/>
    }
}


const mapPropsToState = state => ({
    accountType: state.user.accountType
})

const mapDispatchToProps = dispatch => ({
    loadUser: (token) => dispatch(userActions.loadUserData(token))
})

export default connect(mapPropsToState, mapDispatchToProps)(Home);