import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../../app/user/duck';
import { Link } from 'react-router-dom'
import { Button } from 'react-bootstrap';
import './style.scss';

const Header = (props) => {
    return (
        <div className='header'>
            <h1> {props.title} </h1>
            {props.children}
            <Link className='btn btn-primary' to="/login" onClick={() => props.logout()}> 
                Wyloguj
            </Link>
        </div>
    )
}

const mapDispatchToProps = dispatch => ({
    logout: () => dispatch(userActions.logout())
})
export default connect(null, mapDispatchToProps)(Header);