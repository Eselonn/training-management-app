import React from 'react';
import Container from '../Container';
import TextInput from '../TextInput';
import SubmitButton from '../SubmitButton';
import { connect } from 'react-redux';
import userActions from '../../app/user/duck/actions'
import './style.scss';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state= {
            email: '',
            password: ''
        }
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value })
    }

    handleSubmit(e) {
        e.preventDefault();
        const { email, password } = this.state;
        this.props.userLogin(email, password, this.props.history);
    }

    render() {
        return(
            <Container>
                <form className='login-form' onSubmit={(e) => this.handleSubmit(e)}>
                    <h2> Logowanie </h2>
                    <TextInput 
                        name='email' 
                        onChange={(e) => this.handleChange(e)}  
                        placeholder='Wpisz swoj email...' 
                        title='Email:' 
                        type='text'/>
                    <TextInput 
                        name='password' 
                        onChange={(e) => this.handleChange(e)}  
                        placeholder='Wpisz swoje haslo...' 
                        title='Hasło:'
                        type='password'/>
                    <SubmitButton
                        value='Zaloguj!'/>
                    {this.props.errorDescription !== '' ? 
                    <div className="alert alert-danger">
                        {this.props.errorDescription}
                    </div>
                    : null}
                </form>
            </Container>
        )
    }
}

const mapStateToProps = state => ({
    errorDescription: state.user.error
})

const mapDispatchToProps = dispatch => ({
    userLogin: (email, password, history) => dispatch(userActions.login(email, password, history))
})

export default connect(mapStateToProps, mapDispatchToProps)(Login);