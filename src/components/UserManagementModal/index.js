import React from "react";
import Modal from '../Modal';
import { Form, Button } from 'react-bootstrap'
import { connect } from 'react-redux';
import {userActions} from '../../app/user/duck'
import { modalActions } from '../../app/modal/duck'
import UserList from '../UserTable';

class UserManagement extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
        }
        this.handleChange = this.handleChange.bind(this);
    }

    addDeleteOption(userList) {
        if(!userList) return;
        const newList = [];
        for(let i = 0; i<userList.length; i++) 
            newList.push(Object.assign({}, userList[i], {option: <Button onClick={() => this.handleDeleteUser(userList[i])} variant='danger'> Usuń </Button>}));
        return newList;
    }

    handleDeleteUser(user) {
        const userList = this.props.userList;
        const newUserList = [];
        for(let i=0; i<userList.length; i++)
            if(userList[i].email !== user.email)
                newUserList.push(userList[i]);
        
        this.props.deleteUserFromApp(user, newUserList);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value })
    }

    addNewUser(e) {
        e.preventDefault();
        const userList = this.props.userList;
        this.props.addNewUser({email: this.state.email, active: 'Niezarejestrowany'}, [...userList, {email: this.state.email, active: 'Niezarejestrowany'}])
        this.setState({email: ''});
    }

    render() {
        return (
            <Modal
                title="Zarzadzanie uzytkownikami" 
                modalId='user-management-modal'>
                    <Form onSubmit={(e) => this.addNewUser(e)}>
                        <Form.Group>
                            <Form.Label>Dodaj użytkownika: </Form.Label>
                            <Form.Control type="email" name="email" placeholder="Wprowadz email" onChange={(e) => this.handleChange(e)}/>
                        </Form.Group>
                        <Form.Group>
                            <Button className="default-button-color" type="submit">
                                Dodaj
                            </Button>
                        </Form.Group>

                    </Form>
                    <h3>Lista użytkowników</h3>
                    <UserList userList={this.addDeleteOption(this.props.userList)}/>
            </Modal>
        )
    }

}

const mapStateToProps = state => ({
    userList: state.user.trainingList.allUsers
})

const mapDispatchToProps = dispatch => ({
    createTraining: (training) => dispatch(userActions.addTraining(training)),
    hideModal: () => dispatch(modalActions.closeModal()),
    deleteUserFromApp: (user, newList) => dispatch(userActions.removeUserFromApp(user, newList)),
    addNewUser: (user, newList) => dispatch(userActions.addNewUser(user, newList)) 
})

export default connect(mapStateToProps, mapDispatchToProps)(UserManagement)