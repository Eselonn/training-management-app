import React from 'react';
import Header from '../Header';
import TreningTable from '../TrainingTable'
import Dropdown from '../UserDropdown';
import UserModal from '../UserModal';
import { connect } from 'react-redux';
class UserPanel extends React.Component {

    addUserOptions(trainingList) {
        const newTrainingList = [];
        for(let i = 0; i<trainingList.length; i++)
            if(trainingList[i].status === 'Aktywny')
                newTrainingList.push(Object.assign({}, trainingList[i], {options: <Dropdown training={trainingList[i]}/>}));
            else newTrainingList.push(trainingList[i]);
        return newTrainingList;
    }

    render() {
    return (
        <React.Fragment>
            <Header
                title="Zacznij trening!">
            </Header>
            <div className='table-container'>
                <h2>Lista treningów</h2>
                <TreningTable adminColumn={false} trainingList={this.addUserOptions(this.props.userTraining)}/>
            </div>
            <UserModal/>
        </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    userTraining: state.user.trainingList
})


export default connect(mapStateToProps, null)(UserPanel);