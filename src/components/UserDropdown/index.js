import React from 'react';
import { Dropdown } from 'react-bootstrap'
import { connect } from 'react-redux';
import actions from '../../app/user/duck/actions';
import {modalActions} from '../../app/modal/duck';

class UserDropdown extends React.Component {

    closeTraining(selectedTraining) {
       const allTraining = this.props.userTraining;
       const found = allTraining.find(training => training === selectedTraining);
       found.status = "Zakończony";
       this.props.closeTraining(allTraining, selectedTraining);
    }

    render() {
        return (
            <Dropdown>
            <Dropdown.Toggle variant='secondary' id="dropdown-basic"> Opcje </Dropdown.Toggle>
                <Dropdown.Menu>
                    <Dropdown.Item  className="dropdown-item" onClick={() => this.props.openModal('user-modal-realisation', this.props.training )}> Uzupełnij </Dropdown.Item >
                    <Dropdown.Item  className="dropdown-item" onClick={() => this.closeTraining(this.props.training)}> Zakończ </Dropdown.Item >
                </Dropdown.Menu>
            </Dropdown>
        )
    }

}

const mapStateToProps = state => ({
    userTraining: state.user.trainingList
})

const mapDispatchToProps = dispatch => ({
    closeTraining: (allTrainings, selected) => dispatch(actions.closeTraining(allTrainings, selected)),
    openModal: (modalId, modalProps) => dispatch(modalActions.openModal(modalId, modalProps))

})

export default connect(mapStateToProps, mapDispatchToProps)(UserDropdown);