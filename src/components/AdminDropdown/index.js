import React from 'react';
import { Dropdown } from 'react-bootstrap'
import { connect } from 'react-redux';
import actions from '../../app/user/duck/actions';
import {modalActions} from '../../app/modal/duck';

class AdminDropdown extends React.Component {


    render() {
        const {adminCloseTraining, openModal, deleteTraining} = this.props;
        return (
            <Dropdown>
            <Dropdown.Toggle variant='secondary' id="dropdown-basic"> Opcje </Dropdown.Toggle>
                <Dropdown.Menu>
                    {this.props.training.status !== 'Zakonczony' ? 
                    <Dropdown.Item  className="dropdown-item" onClick={() => openModal('edit-training-modal', this.props.training )}> Edytuj </Dropdown.Item >: null}
                    {this.props.training.status !== 'Zakonczony' ? 
                        <Dropdown.Item  className="dropdown-item" onClick={() => adminCloseTraining(this.props.training)}> Zakoncz </Dropdown.Item > : null}
                    <Dropdown.Item  className="dropdown-item" onClick={() => deleteTraining(this.props.training)}> Usun </Dropdown.Item >
                </Dropdown.Menu>
            </Dropdown>
        )
    }

}

const mapStateToProps = state => ({
    userTraining: state.user.trainingList
})

const mapDispatchToProps = dispatch => ({
    adminCloseTraining: (training) => dispatch(actions.adminCloseTraining(training)),
    openModal: (modalId, modalProps) => dispatch(modalActions.openModal(modalId, modalProps)),
    deleteTraining: (training) => dispatch(actions.deleteTraining(training))

})

export default connect(mapStateToProps, mapDispatchToProps)(AdminDropdown);