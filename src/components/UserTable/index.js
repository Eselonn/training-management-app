import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';

class UserTable extends React.Component {
    render() {
        const columns = [{
            dataField: 'id',
            hidden: true
        }, {
            dataField: 'name',
            text: 'Uzytkownik'
        }, {
            dataField: 'email',
            text: 'Email'
        }, {
            dataField: 'active',
            text: 'Status'
        }, {
            dataField: 'option',
        }
    ]

        return (
            <BootstrapTable 
                striped  
                keyField='email' 
                data={this.props.userList} 
                columns={columns} 
                pagination={  paginationFactory({ 
                    sizePerPageList: [ {
                        text: '5', value: 5
                      }, {
                        text: '10', value: 10
                      }, {
                        text: '25', value: 25
                      } ]

                }) } />
        )
    }
}

export default UserTable;